<?php
include_once 'vendor/autoload.php';

use labApps\tree;

$objtree = new tree();
$data = $objtree->Viewuser();
//print_r($data);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="style.css" type="text/css">
    </head>
    <body>
        <div class="tree">
            <ul>
                <li>
                    <?php
                foreach ($data as $rfvalue) {
                    if($rfvalue['Referenced_id']==0){ ?>
                <a href="#"><?php echo $rfvalue['Name'];?></a>
                <?php } }?>
                    <ul>
                        <?php
                        foreach ($data as $rfvalue) {
                    if($rfvalue['Referenced_id']==1){ ?>
                        <li>
                            <a href="#"><?php echo $rfvalue['Name'];?></a>
                            <?php 
                                $grandChild=$objtree->grandchildView();
                            ?>
                            <?php  foreach ($grandChild as $gdvalue){ 
                                if($gdvalue['id']==$gdvalue['Referenced_id']){  ?>
                            <ul> 
                                <li> 
                                    <a href="#"><?php echo $gdvalue['Name'];?></a>
                                </li>  
                            </ul>
                             <?php } }?>
                        </li>
                    <?php } }?>
                    </ul>
                </li>
            </ul>
        </div>
    </body>
</html>
