<?php
include_once 'vendor/autoload.php';
use labApps\tree;
use labApps\Lab\Schedule\Schedule;
use labApps\Lab\Software\Software;
use labApps\Lab\LabInfo\LabInfo;
use labApps\Lab\Course\Course;


$tree=new tree();
?>
<script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    
<form action="ScheduleStore.php" method="Post">

    <table>
        <tr>
            <td>Course Name <span style="color:red">*</span><br>
                <select style="width: 100%;" name="courseId">
                    <?php if (isset($_SESSION['AllDAta']['courseId'])) { ?>
                        <option><?php echo $_SESSION['AllDAta']['courseId'];
                        unset($_SESSION['AllDAta']['courseId']); ?></option> 
                    <?php } ?>
                    <option>Select Course Name</option>
                    <?php
                    $courseObj = new Course();
                    $data = $courseObj->ViewAllcourses();
                    foreach ($data as $value) {
                        ?>
                        <option value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                    <?php }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Assistant Trainer <span style="color:red"></span><br>
                <ul>
                    <li id="assTrainer"></li>
                </ul>
            </td>
        </tr>
    </table>

    <select name="somthing" onchange="getId(this.value)">
        <?php
        $data1=$tree->Viewuser();
        foreach ($data1 as $value){ ?>
        <option value="<?php echo $value['id']; ?>"><?php echo $value['Name']; ?></option>
        <?php } ?>
    </select>
    
    <ul>
        <li id="changevalue"></li>
    </ul>

</form>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

<script>
                                    function getId(val) {
                                        $.ajax({
                                            type: "post",
                                            url: "Ajax.php",
                                            data: "Referenced_id=" + val,
                                            success: function (data) {
                                                $("#changevalue").html(data);
                                            }

                                        });
                                    }
</script>