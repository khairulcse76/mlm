<?php
namespace labApps\Lab\Schedule;
use PDO;

class Schedule {
   public $id = '';
    public $unique_id = '';
    public $courseId = '';
    public $batch_no = '';
    public $lead_trainer = '';
    public $user = 'root';
    public $asst_trainer = '';
    public $lab_asst = '';
    public $lab_id = '';
    public $start_date = '';
    public $ending_date = '';
    public $day = '';
    public $is_running = '';
    public $assigned_by = '';
    public $pass = '';
    public $connection = '';
    public $Assignby='';
    public $strtH='';
    public $strtM='';
    public $endH='';
    public $endM='';
    


    public function __construct() {
        session_start();
       date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=lab', $this->user, $this->pass);
        
    }
    public function prepare($data='')
    {
//        echo '<pre>';
//print_r($_POST);
//die(); 
    if (array_key_exists('strtH', $data)) {
            $this->strtH = $data['strtH'];
        }
        if (array_key_exists('strtM', $data)) {
            $this->strtM = $data['strtM'];
        }
        if (array_key_exists('endH', $data)) {
            $this->endH = $data['endH'];
        }
        if (array_key_exists('endM', $data)) {
            $this->endM = $data['endM'];
        }
        if (array_key_exists('courseId', $data)) {
            $this->courseId = $data['courseId'];
        }
        if (array_key_exists('labid', $data)) {
            $this->lab_id = $data['labid'];
        }
        if (array_key_exists('labassistant', $data)) {
            $this->lab_asst = $data['labassistant'];
        }
        if (array_key_exists('trainerId', $data)) {
            $this->lead_trainer = $data['trainerId'];
        }
        if (array_key_exists('assTrainer', $data)) {
            $this->asst_trainer = $data['assTrainer'];
        }
        if (array_key_exists('strtDate', $data)) {
            $this->start_date = $data['strtDate'];
        }
        if (array_key_exists('endDate', $data)) {
            $this->ending_date = $data['endDate'];
        }
        if (array_key_exists('days', $data)) {
            $this->day = $data['days'];
        }
        if (array_key_exists('strttime', $data)) {
            $this->start_time = $data['strttime'];
        }
        if (array_key_exists('endTime', $data)) {
            $this->ending_time = $data['endTime'];
        }
        if(array_key_exists('unique_id', $data))
       {
           $this->unique_id=$data['unique_id'];
       }
        if(array_key_exists('id', $data))
       {
           $this->id=$data['id'];
       }
       if(array_key_exists('Assignby', $data))
       {
           $this->Assignby=$data['Assignby'];
       }
       
       if(array_key_exists('BatchNo', $data))
       {
           $this->batch_no=$data['BatchNo'];
       }
       
        return $this;
    }
       
 public function ViewAllSchedule()
    {
           $query="SELECT course_trainer_lab_mapping.*, courses.title
           FROM course_trainer_lab_mapping
           INNER JOIN courses
           ON courses.id = course_trainer_lab_mapping.course_id
           ORDER BY course_trainer_lab_mapping.id DESC";           
            $stmt=  $this->connection->query($query);
             $stmt->execute();
             $table=$stmt->fetchAll();
             return $table;
    }
    public function Status(){
         try {
             $date=date("Y-m-d h:i:sA");
             
              $sql = "UPDATE course_trainer_lab_mapping SET is_running='0', updated='$date' WHERE unique_id="."'".$this->unique_id."'";
//               echo $sql;die();
              $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                    $_SESSION['update_msg'] = '<b style="color: green;">Course Disable</b>';
                    header('location:Overview.php');
                }
        } catch (Exception $e) {
            
        }
    }
    public function StatusOFF(){
         try {
             $date=date("Y-m-d h:i:sA");
             
              $sql = "UPDATE course_trainer_lab_mapping SET is_running='1', updated='$date' WHERE unique_id="."'".$this->unique_id."'";
//               echo $sql;die();
              $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                    $_SESSION['update_msg'] = '<b style="color: green;">Course now Running</b>';
                    header('location:Overview.php');
                }
        } catch (Exception $e) {
            
        }
    }

//    ."AND start_time="."'".$this->start_time."'"
//            ."'"."AND start_time="."'".$this->start_time
   public function ScheduleStore() {
try {
        $datequery="SELECT * FROM `course_trainer_lab_mapping` WHERE start_date="."'".$this->start_date."'";
        $datstmt = $this->connection-> prepare($datequery);
        $datstmt -> execute();
        $date = $datstmt ->fetchAll();
        
        $batchquery="SELECT * FROM `course_trainer_lab_mapping` WHERE batch_no="."'".$this->batch_no."'";
        $batchstmt = $this->connection-> prepare($batchquery);
        $batchstmt -> execute();
        $checkBatch = $batchstmt -> fetch();
        
        if(is_array($checkBatch)){
            $_SESSION['error_msg'] = '<b style="color: red;">Batch already exists</b>';
                header('location:ScheduleAdd.php');
      }  else {     
        if(!empty($date))
        {
            $startime= $date['start_time'];
            $startdate= $date['start_date'];
            $endTime= $date['ending_time'];
            $endDate= $date['ending_date'];
            $Days=$date['day'];
            $lab=$date['lab_id'];
            $CheckHour = substr($startime, 0, 1);
            $checkEndHour=substr($endTime, 0, 1);
            
            if($Days==$this->day){
                if($CheckHour>=$checkEndHour){
                while($CheckHour >= $checkEndHour){
                    $loop[]= $CheckHour--;
                        }  
                    }elseif($CheckHour<=$checkEndHour) {
                            while($CheckHour <= $checkEndHour){
                            $loop[]= $CheckHour++;
                        } 
                    }  else {
                        $_SESSION['error_msg'] = '<b style="color: red;">INVALID INPUTE</b>';
                         header('location:ScheduleAdd.php');
                    }
                    if($loop[0]==$this->strtH || $loop[0]==$this->endH || $loop[1]==$this->strtH || $loop[1]==$this->endH
                        || $loop[2]==$this->strtH || $loop[2]==$this->endH || $loop[3]==$this->strtH 
                        || $loop[3]==$this->endH || $loop[4]==$this->strtH || $loop[4]==$this->endH 
                        ||$loop[5]==$this->strtH||$loop[5]==$this->endH){
                    if($lab==$this->lab_id){
                         $_SESSION['error_msg'] = '<b style="color: red;">this time or Lab already exist</b>';
                         header('location:ScheduleAdd.php');
                    }  else {
                            $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `unique_id`, `course_id`, 
                           `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, 
                           `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`)

                            VALUES(:id, :unique_id, :cri, :bno, :ltr, :ast, :las, :lid, :sdate, :edate, :stime, :etime,
                            :d, :isrun, :assigned_by, :created, :updated, :deleted)";
                    //           $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`)VALUES(:id, :cri)";
                                $stmt = $this->connection->prepare($query);

                               $result=$stmt->execute(array(
                                    ':id' => NULL,
                                   ':unique_id'=> uniqid(),
                                    ':cri' => $this->courseId,
                                    ':bno' => $this->batch_no,
                                    ':ltr' => $this->lead_trainer,
                                    ':ast' => $this->asst_trainer,
                                    ':las' => $this->lab_asst,
                                    ':lid' => $this->lab_id,
                                    ':sdate' => $this->start_date,
                                    ':edate' => $this->ending_date,
                                    ':stime' => $this->strtH.":".$this->strtM,
                                    ':etime' => $this->endH.":".$this->endM,
                                    ':d' => $this->day,
                                    ':isrun' =>0,
                                   ':assigned_by'=>  $_SESSION['full_name'],
                                    ':created' => date("Y-m-d h:i:sA"),
                                    ':updated' => date("Y-m-d h:i:sA"),
                                    ':deleted' => date("Y-m-d h:i:sA"),
                                ));
                                if ($result) {
                                    $_SESSION['error_msg'] = '<b style="color: blue;">Course Schedule Inserted</b>';
                                    unset($_SESSION['AllDAta']);
                                    header('location:ScheduleAdd.php');
                                }        
                    }
                }  else {
                   $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `unique_id`, `course_id`, 
                            `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, 
                            `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`)

                             VALUES(:id, :unique_id, :cri, :bno, :ltr, :ast, :las, :lid, :sdate, :edate, :stime, :etime,
                             :d, :isrun, :assigned_by, :created, :updated, :deleted)";
                //           $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`)VALUES(:id, :cri)";
                            $stmt = $this->connection->prepare($query);

                           $result=$stmt->execute(array(
                                ':id' => NULL,
                               ':unique_id'=> uniqid(),
                                ':cri' => $this->courseId,
                                ':bno' => $this->batch_no,
                                ':ltr' => $this->lead_trainer,
                                ':ast' => $this->asst_trainer,
                                ':las' => $this->lab_asst,
                                ':lid' => $this->lab_id,
                                ':sdate' => $this->start_date,
                                ':edate' => $this->ending_date,
                                ':stime' => $this->strtH.":".$this->strtM,
                                ':etime' => $this->endH.":".$this->endM,
                                ':d' => $this->day,
                                ':isrun' =>0,
                               ':assigned_by'=>  $_SESSION['full_name'],
                                ':created' => date("Y-m-d h:i:sA"),
                                ':updated' => date("Y-m-d h:i:sA"),
                                ':deleted' => date("Y-m-d h:i:sA"),
                            ));
                            if ($result) {
                                $_SESSION['error_msg'] = '<b style="color: blue;">Course Schedule Inserted</b>';
                                unset($_SESSION['AllDAta']);
                                header('location:ScheduleAdd.php');
                            }
                } 
            }  else {
                $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `unique_id`, `course_id`, 
                `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, 
                `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`)
                
                 VALUES(:id, :unique_id, :cri, :bno, :ltr, :ast, :las, :lid, :sdate, :edate, :stime, :etime,
                 :d, :isrun, :assigned_by, :created, :updated, :deleted)";
        //           $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`)VALUES(:id, :cri)";
                    $stmt = $this->connection->prepare($query);

                   $result=$stmt->execute(array(
                        ':id' => NULL,
                       ':unique_id'=> uniqid(),
                        ':cri' => $this->courseId,
                        ':bno' => $this->batch_no,
                        ':ltr' => $this->lead_trainer,
                        ':ast' => $this->asst_trainer,
                        ':las' => $this->lab_asst,
                        ':lid' => $this->lab_id,
                        ':sdate' => $this->start_date,
                        ':edate' => $this->ending_date,
                        ':stime' => $this->strtH.":".$this->strtM,
                        ':etime' => $this->endH.":".$this->endM,
                        ':d' => $this->day,
                        ':isrun' =>0,
                       ':assigned_by'=>  $_SESSION['full_name'],
                        ':created' => date("Y-m-d h:i:sA"),
                        ':updated' => date("Y-m-d h:i:sA"),
                        ':deleted' => date("Y-m-d h:i:sA"),
                    ));
                    if ($result) {
                        $_SESSION['error_msg'] = '<b style="color: blue;">Course Schedule Inserted</b>';
                        unset($_SESSION['AllDAta']);
                        header('location:ScheduleAdd.php');
                    }
            }
            
            echo "<br>";
        }  else {
             $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `unique_id`, `course_id`, 
                `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, 
                `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`)
                
                 VALUES(:id, :unique_id, :cri, :bno, :ltr, :ast, :las, :lid, :sdate, :edate, :stime, :etime,
                 :d, :isrun, :assigned_by, :created, :updated, :deleted)";
//           $query = "INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`)VALUES(:id, :cri)";
            $stmt = $this->connection->prepare($query);
            
           $result=$stmt->execute(array(
                ':id' => NULL,
               ':unique_id'=> uniqid(),
                ':cri' => $this->courseId,
                ':bno' => $this->batch_no,
                ':ltr' => $this->lead_trainer,
                ':ast' => $this->asst_trainer,
                ':las' => $this->lab_asst,
                ':lid' => $this->lab_id,
                ':sdate' => $this->start_date,
                ':edate' => $this->ending_date,
                ':stime' => $this->strtH.":".$this->strtM,
                ':etime' => $this->endH.":".$this->endM,
                ':d' => $this->day,
                ':isrun' =>0,
               ':assigned_by'=>  $_SESSION['full_name'],
                ':created' => date("Y-m-d h:i:sA"),
                ':updated' => date("Y-m-d h:i:sA"),
                ':deleted' => date("Y-m-d h:i:sA"),
            ));
            if ($result) {
                $_SESSION['error_msg'] = '<b style="color: blue;">Course Schedule Inserted</b>';
                unset($_SESSION['AllDAta']);
                header('location:ScheduleAdd.php');
            }
        }
           
       
        }
}catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    

    

    public function ScheduleUpdate() {
try {
        $datequery="SELECT * FROM `course_trainer_lab_mapping` WHERE start_date="."'".$this->start_date."'";
        $datstmt = $this->connection-> prepare($datequery);
        $datstmt -> execute();
        $date = $datstmt -> fetch();
        
        $batchquery="SELECT * FROM `course_trainer_lab_mapping` WHERE batch_no="."'".$this->batch_no."'";
        $batchstmt = $this->connection-> prepare($batchquery);
        $batchstmt -> execute();
        $checkBatch = $batchstmt -> fetch();
        
        
               $daterime=date("Y-m-d h:i:sA");
              $assignby=$_SESSION['full_name'];
       
             $updateQuery= "UPDATE `course_trainer_lab_mapping` SET `course_id` = '$this->courseId', 
            `batch_no` = '$this->batch_no', `lead_trainer` = '$this->lead_trainer', `asst_trainer` = '$this->asst_trainer', 
            `lab_asst` = '$this->lab_asst', `lab_id` = '$this->lab_id', `start_date` = '$this->start_date',
            `ending_date` = '$this->ending_date', `start_time` = '$this->strtH:$this->strtM', `ending_time` = '$this->endH:$this->endM', `day` = '$this->day', 
            `is_running` = '1', `assigned_by` = '$assignby', 
            `updated` = '$daterime'
            WHERE `course_trainer_lab_mapping`.`unique_id`="."'".$this->unique_id."'";
            $stmt = $this->connection->prepare($updateQuery);
            $result=$stmt->execute();
                
             
          
        if(!empty($date))
        { 
            $startime= $date['start_time'];
            $startdate= $date['start_date'];
            $endTime= $date['ending_time'];
            $endDate= $date['ending_date'];
            $Days=$date['day'];
            $lab=$date['lab_id'];
            $CheckHour = substr($startime, 0, 1);
            $checkEndHour=substr($endTime, 0, 1);
            
            if($Days==$this->day){
                if($CheckHour>=$checkEndHour){
                while($CheckHour >= $checkEndHour){
                    $loop[]= $CheckHour--;
                        }  
                    }elseif($CheckHour<=$checkEndHour) {
                            while($CheckHour <= $checkEndHour){
                            $loop[]= $CheckHour++;
                        } 
                    }  else {
                        $_SESSION['error_msg'] = '<b style="color: red;">INVALID INPUTE</b>';
                         header('location:ScheduleEdit.php');
                    }
                    if($loop[0]==$this->strtH || $loop[0]==$this->endH || $loop[1]==$this->strtH || $loop[1]==$this->endH
                        || $loop[2]==$this->strtH || $loop[2]==$this->endH || $loop[3]==$this->strtH 
                        || $loop[3]==$this->endH || $loop[4]==$this->strtH || $loop[4]==$this->endH 
                        ||$loop[5]==$this->strtH||$loop[5]==$this->endH){
                    if($lab==$this->lab_id){
                         $_SESSION['error_msg'] = '<b style="color: red;">this time or Labe already exist</b>';
                         header('location:ScheduleEdit.php');
                    }  else {
                           if($result)
                            {
                                $_SESSION['error_msg']='<b style=" color: green;">Data Update Successful</b>';
                                unset($_SESSION['AllDAta']);
                                 header("location:ScheduleView.php?unique_id=$this->unique_id");
                            } 
                    }
                }  else {
                    if($result)
                        {
                            $_SESSION['error_msg']='<b style=" color: green;">Data Update Successful</b>';
                            unset($_SESSION['AllDAta']);
                             header("location:ScheduleView.php?unique_id=$this->unique_id");
                        }
                   } 
            }  else {
                if($result)
                    {
                        $_SESSION['error_msg']='<b style=" color: green;">Data Update Successful</b>';
                        unset($_SESSION['AllDAta']);
                          header("location:ScheduleView.php?unique_id=$this->unique_id");
                    }
            }
            
        
        }
}catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
   
    
    
       
    public function Scheduleshow()
    {
         $query="SELECT course_trainer_lab_mapping.*, courses.title
           FROM course_trainer_lab_mapping
           INNER JOIN courses
           ON courses.id = course_trainer_lab_mapping.course_id
           WHERE `course_trainer_lab_mapping`.`unique_id`="."'".$this->unique_id."'".
           "ORDER BY course_trainer_lab_mapping.id DESC";
          $stmt=  $this->connection->query($query);
            $stmt->execute();
            $row=$stmt->fetch();
            return $row;
    }
    public function ScheduleEditPageshow()
    {
         $query="SELECT course_trainer_lab_mapping.*, courses.title
           FROM course_trainer_lab_mapping
           INNER JOIN courses
           ON courses.id = course_trainer_lab_mapping.course_id
           WHERE `course_trainer_lab_mapping`.`unique_id`="."'".$this->unique_id."'"
           ."ORDER BY course_trainer_lab_mapping.id DESC";
          $stmt=  $this->connection->query($query);
            $stmt->execute();
            $row=$stmt->fetch();
            return $row;
    }
    
    
    
        public function logout()
    {
            unset( $_SESSION['emty_msg']);
            unset($_SESSION['user'] );
            unset($_SESSION['login_msg']);
            header('location:login.php');
    }
    
    
    
    public function ScheduleDelete()
    {
        try {
              $query = "DELETE FROM `course_trainer_lab_mapping` WHERE `course_trainer_lab_mapping`.`unique_id`="."'".$this->unique_id."'";
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                 $_SESSION['update_msg'] = '<b style="color: red;">Successfully deleted</b>';
                header('location:Overview.php');
                }    
        } catch (Exception $ex) {
            
        }
    }

    

    public function userprofile()
    {
       $query="SELECT * FROM profiles WHERE `profiles`.`users_id` =".$_SESSION['userid'];
       $stmt=  $this->connection->query($query);
       $stmt->execute();
       $fulltable=$stmt->fetch();
       return $fulltable;
    }

    
}

