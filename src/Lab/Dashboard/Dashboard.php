<?php
namespace labApps\Lab\Dashboard;
use PDO;

class dashboard{
   public $id = '';
    public $unique_id = '';
    public $courseId = '';
    public $batch_no = '';
    public $lead_trainer = '';
    public $user = 'root';
    public $asst_trainer = '';
    public $lab_asst = '';
    public $lab_id = '';
    public $start_date = '';
    public $ending_date = '';
    public $day = '';
    public $is_running = '';
    public $assigned_by = '';
    public $pass = '';
    public $connection = '';
    public $Assignby='';
    public $strtH='';
    public $strtM='';
    public $endH='';
    public $endM='';
    


    public function __construct() {
        session_start();
       date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=lab', $this->user, $this->pass);
        
    }
    public function prepare($data='')
    {
//        echo '<pre>';
//print_r($_POST);
//die(); 
    if (array_key_exists('strtH', $data)) {
            $this->strtH = $data['strtH'];
        }
        if (array_key_exists('strtM', $data)) {
            $this->strtM = $data['strtM'];
        }
        if (array_key_exists('endH', $data)) {
            $this->endH = $data['endH'];
        }
        if (array_key_exists('endM', $data)) {
            $this->endM = $data['endM'];
        }
        if (array_key_exists('courseId', $data)) {
            $this->courseId = $data['courseId'];
        }
        if (array_key_exists('labid', $data)) {
            $this->lab_id = $data['labid'];
        }
        if (array_key_exists('labassistant', $data)) {
            $this->lab_asst = $data['labassistant'];
        }
        if (array_key_exists('trainerId', $data)) {
            $this->lead_trainer = $data['trainerId'];
        }
        if (array_key_exists('assTrainer', $data)) {
            $this->asst_trainer = $data['assTrainer'];
        }
        if (array_key_exists('strtDate', $data)) {
            $this->start_date = $data['strtDate'];
        }
        if (array_key_exists('endDate', $data)) {
            $this->ending_date = $data['endDate'];
        }
        if (array_key_exists('days', $data)) {
            $this->day = $data['days'];
        }
        if (array_key_exists('strttime', $data)) {
            $this->start_time = $data['strttime'];
        }
        if (array_key_exists('endTime', $data)) {
            $this->ending_time = $data['endTime'];
        }
        if(array_key_exists('unique_id', $data))
       {
           $this->unique_id=$data['unique_id'];
       }
        if(array_key_exists('id', $data))
       {
           $this->id=$data['id'];
       }
       if(array_key_exists('Assignby', $data))
       {
           $this->Assignby=$data['Assignby'];
       }
       
       if(array_key_exists('BatchNo', $data))
       {
           $this->batch_no=$data['BatchNo'];
       }
       
        return $this;
    }
       
 public function ViewAllSchedule()
    {
           $query="SELECT course_trainer_lab_mapping.*, courses.title
           FROM course_trainer_lab_mapping
           INNER JOIN courses
           ON courses.id = course_trainer_lab_mapping.course_id
           WHERE course_trainer_lab_mapping.is_running=1
           ORDER BY course_trainer_lab_mapping.id DESC  LIMIT 5";           
            $stmt=  $this->connection->query($query);
             $stmt->execute();
             $table=$stmt->fetchAll();
             return $table;
    }
    public function UpcomingCourse()
    {
           $query="SELECT * From courses WHERE is_offer='0' ORDER BY courses.is_offer DESC LIMIT 5";           
            $stmt=  $this->connection->query($query);
             $stmt->execute();
             $table=$stmt->fetchAll();
             return $table;
    }
      public function LoginTime()
    {
          $id=$_SESSION['id'];
           $query="SELECT * From users WHERE users.id='$id'";           
            $stmt=  $this->connection->query($query);
             $stmt->execute();
             $table=$stmt->fetch();
             return $table;
    }
    public function TrainerView()
    {
          $id=$_SESSION['id'];
           $query="SELECT * From trainers WHERE trainer_status='Lead Trainer' ORDER BY trainers.id DESC LIMIT 2";           
            $stmt=  $this->connection->query($query);
             $stmt->execute();
             $table=$stmt->fetchAll();
             return $table;
    }
     public function ResentCompletedCourse()
    {
           $query="SELECT course_trainer_lab_mapping.*, courses.title
           FROM course_trainer_lab_mapping
           INNER JOIN courses
           ON courses.id = course_trainer_lab_mapping.course_id
           WHERE course_trainer_lab_mapping.is_running=0
           ORDER BY course_trainer_lab_mapping.id DESC  LIMIT 5";           
            $stmt=  $this->connection->query($query);
             $stmt->execute();
             $table=$stmt->fetchAll();
             return $table;
    }
    
    public function Status(){
         try {
             $date=date("Y-m-d h:i:sA");
             
              $sql = "UPDATE course_trainer_lab_mapping SET is_running='0', updated='$date' WHERE unique_id="."'".$this->unique_id."'";
//               echo $sql;die();
              $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                    $_SESSION['update_msg'] = '<b style="color: green;">Course Disable</b>';
                    header('location:Overview.php');
                }
        } catch (Exception $e) {
            
        }
    }
    public function StatusOFF(){
         try {
             $date=date("Y-m-d h:i:sA");
             
              $sql = "UPDATE course_trainer_lab_mapping SET is_running='1', updated='$date' WHERE unique_id="."'".$this->unique_id."'";
//               echo $sql;die();
              $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                    $_SESSION['update_msg'] = '<b style="color: green;">Course now Running</b>';
                    header('location:Overview.php');
                }
        } catch (Exception $e) {
            
        }
    }

//    ."AND start_time="."'".$this->start_time."'"
//            ."'"."AND start_time="."'".$this->start_time
   }
