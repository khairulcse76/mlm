<?php

namespace labApps\Lab\Course;
use PDO;

class Course {
    public $id='';
    public $unique_id='';
    public $username='';
    public $email='';
    public $user='root';
    public $password='';
    public $pass='';
    public $connection='';
    public $FullName='';
    public $image='';




    public function __construct() {
        session_start();
       date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=lab', $this->user, $this->pass);
        
    }
    public function prepare($data='')
    {
       if(array_key_exists('userName', $data))
       {
           $this->username=$data['userName'];
       }
       if(array_key_exists('password', $data))
       {
           $this->password=$data['password'];
       }
       if(array_key_exists('email', $data))
       {
           $this->email=$data['email'];
       }
       
        if(array_key_exists('fullName', $data))
       {
           $this->FullName=$data['fullName'];
       }
        if(array_key_exists('unique_id', $data))
       {
           $this->unique_id=$data['unique_id'];
       }
        if(array_key_exists('id', $data))
       {
           $this->id=$data['id'];
       }
       
       
        
        return $this;
    }
    
    public function  CourseStore()
    {
       
     $query="SELECT * FROM `users` WHERE username="."'".$this->username."'";
    $STH = $this->connection-> prepare($query);
    $STH -> execute();
    $result = $STH ->fetch();
        
       
       $queremail="SELECT * FROM `users` WHERE email="."'".$this->email."'";
       $stmemail=  $this->connection->query($queremail);
       $stmemail->execute();
       $result2 = $stmemail->fetch();

           if(is_array($result)){
           $_SESSION['error_msg']= '<b style="color: blue;">username alrady exists..!</b>';
            header('location:UserAdd.php');
        } elseif(is_array($result2)){
            $_SESSION['error_msg']= '<b style="color: blue;">This Eamil alrady Used!</b>';
            header('location:UserAdd.php');
        }else{
            try {
                 
                       
                $qery = "INSERT INTO `users` (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`, `updated`, `deleted`)
                                         VALUES(:id, :uni, :full_name, :username, :email, :password,
                                         :image, :is_active, :is_admin, :is_delete, :created, :updated, :deleted)";
                   $stmt=  $this->connection->prepare($qery);
                   $result=$stmt->execute(array(
                    ':id' => Null,
                    ':uni' => uniqid(),
                   ':full_name' => $this->FullName,
                    ':username' => $this->username,
                    ':email' => $this->email,
                   ':password' => $this->password,
                   ':image' => $this->image,
                   ':is_active' => 0,
                   ':is_admin' => 0,
                   ':is_delete' => 0,
                   ':created' => date("Y-m-d h:i:sA"),
                   ':updated' => date("Y-m-d h:i:sA"),
                   ':deleted' => date("Y-m-d h:i:sA"),
                   ':deleted' => date("Y-m-d h:i:sA"),
                       ));
                       if ($result)
                       {
//                           $userid=$this->connection->lastInsertId();
//                           $query1 = "INSERT INTO  profiles(`id`, `users_id`)
//                    VALUES(:id,  :userid)";
//                   $stmt=  $this->connection->prepare($query1);
//                   $result=$stmt->execute(array(
//                    ':id' => Null,
//                    ':userid' => $userid,
//                       
//                       ));
                   $_SESSION['id']=$userid;
                    $_SESSION['error_msg']= '<b style="color: blue;">Successfully User Inserted</b>';
                    unset($_SESSION['AllDAta']);
                    header('location:UserAdd.php');
                       }
            } catch (Exception $ex) {
                
            }
        }
        
    }
    
     public function loginuser()
       {
           try {
               $query= "SELECT * FROM users WHERE username="."'".$this->username."' "."AND password="."'".$this->password."'";
               $stmt=  $this->connection->prepare($query);
               $stmt->execute();
               $userdata=$stmt->fetch();
               echo "<pre>";
//               print_r($userdata);
               
               if($userdata)
               {
                   $_SESSION['username']=  $this->username;
                   $_SESSION['user'] = $userdata;
                  $_SESSION['userid'] = $userdata['id'];
//                  print_r($_SESSION['userid']);
//                  die();
                   $_SESSION['login_msg']='<b style=" color:blue; font-size: 16px; ">Login Successfull <br>Welcome this website';
               } 
               return $userdata;
           } catch (Exception $e) {
               $_SESSION['emty_msg']="request error....!!!";
                header('location:login.php');
           }
       }
       
    public function User_show()
    {
        $qry="SELECT * FROM users WHERE unique_id="."'".$this->unique_id."'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $row=$stmt->fetch();
       return $row;
    }
       public function ViewAllcourses()
    {
       $qry="SELECT * FROM courses";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
        public function logout()
    {
            unset( $_SESSION['emty_msg']);
            unset($_SESSION['user'] );
            unset($_SESSION['login_msg']);
            header('location:login.php');
    }
    
//     $query=  "UPDATE `profiles` SET `users_id` = '100', `first_name` = 'khairull', `last_name` = 'islaml', 
//                   `personal_mobile` = '1784983611', `home_phone` = '1789456894', `address` = 'nothingl', `nationality` = 'bangladeshil',
//                    `Religion` = 'inlaml', `blode_group` = 'B+l', `gender` = 'Malel', `picture` = 'nothingl', 
//                    `dateofbirth` = '09/11/1994l', `nationalID` = 'nothingl', `lastEducationalStatus` = 'csel', `occupation` = 'Employeel', 
//                    `created` = '2:11:44 a', `modified` = '2:11:44 a', `deleted` = '2:11:44 a' WHERE `profiles`.`id` = 1; ";
    
    public function UserUpdate()
    {   
        try {
//              UPDATE `users` SET `full_name` = 'khairul islam a', `email` = 'khairulislamtpi76@gmail.coma', `password` = 'khaiirula', `image` = 'a' WHERE `users`.`id` = 5;


              $qurey = "UPDATE `users` SET `full_name` = '$this->FullName', `email` = '$this->email', `password` = '$this->password', `image` = '$this->image' WHERE `users`.`unique_id`="."'".$this->unique_id."'";
               
              $stmt = $this->connection->prepare($qurey);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['update_msg']='<b style=" color: green;">Data Update Successful</b>';
                unset($_SESSION['AllDAta']);
                header("location:Userlist.php");
                }
        } catch (Exception $e) {
            
        }
       
    }
    
    
    public function UserDelete()
    {
        try {
              $query = "DELETE FROM `users` WHERE `users`.`unique_id`="."'".$this->unique_id."'";
//              echo $query;
//              die();
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                    $_SESSION['update_msg']='<b style=" color: red;">User Successfully Deleted</b>';
                    header("location:Userlist.php");
                }    
        } catch (Exception $ex) {
            
        }
    }

    

    public function userprofile()
    {
       $query="SELECT * FROM profiles WHERE `profiles`.`users_id` =".$_SESSION['userid'];
       $stmt=  $this->connection->query($query);
       $stmt->execute();
       $fulltable=$stmt->fetch();
       return $fulltable;
    }

    
}
