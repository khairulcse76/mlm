<?php

namespace labApps;

use PDO;

class tree {

    public $id = '';
    public $unique_id = '';
    public $username = '';
    public $email = '';
    public $user = 'root';
    public $password = '';
    public $pass = '';
    public $connection = '';
    public $FullName = '';
    public $image = '';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=mlm', $this->user, $this->pass);
    }

    public function prepare($data = '') {

        return $this;
    }

    public function Viewuser() {

        $query = "SELECT * FROM `users`";
        $STH = $this->connection->prepare($query);
        $STH->execute();
        $result = $STH->fetchAll();

        return $result;
    }

    public function grandchildView() {
        $query = "SELECT `id` FROM `users` WHERE Referenced_id=1";
        $STH = $this->connection->prepare($query);
        $STH->execute();
        $result = $STH->fetchAll();
//        print_r($result);
        foreach ($result as $idvalue) {
            if (!empty($idvalue)) {
                $query1 = "SELECT * FROM `users` WHERE Referenced_id=" . "'" . $idvalue['id'] . "'";
                $STH1 = $this->connection->prepare($query1);
                $STH1->execute();
                $result1 = $STH1->fetchAll();
            }
        }return $result1;
    }

}
